// How to congigure https://www.terraform.io/docs/providers/aws/index.html
provider "aws" {
  region = "us-east-1"
  shared_credentials_file = "/Users/alex.shakun/.aws/credentials"
  profile                 = "personal"
}

resource "aws_lambda_function" "example" {
  function_name = "ServerlessExample"

  s3_bucket = "terraform-serverless-example-shakun"
  s3_key    = "v1.0.0/example.zip"

  handler = "index.handler"
  runtime = "nodejs10.x"

  role = "${aws_iam_role.lambda_exec.arn}"
}

# IAM role which dictates what other AWS services the Lambda function
# may access.
resource "aws_iam_role" "lambda_exec" {
  name = "serverless_example_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "s3_policy" {
  name = "s3_policy"
  role = "${aws_iam_role.lambda_exec.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Action": [
        "logs:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}


resource "aws_cloudwatch_event_rule" "every_five_minutes" {
    name = "every_five_minutes"
    description = "Fires every five minutes"
    schedule_expression = "rate(5 minutes)"
    # is_enabled = true
}

resource "aws_cloudwatch_event_target" "example_every_five_minutes" {
    rule = "${aws_cloudwatch_event_rule.every_five_minutes.name}"
    target_id = "${aws_lambda_function.example.function_name}"
    arn = "${aws_lambda_function.example.arn}"
}

resource "aws_lambda_permission" "example_lambda_every_five_minutes" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.example.function_name}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.every_five_minutes.arn}"
}